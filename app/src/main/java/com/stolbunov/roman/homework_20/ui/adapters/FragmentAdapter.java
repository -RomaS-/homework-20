package com.stolbunov.roman.homework_20.ui.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.stolbunov.roman.homework_20.ui.fragmants.AirFragment;
import com.stolbunov.roman.homework_20.ui.fragmants.FireFragment;
import com.stolbunov.roman.homework_20.ui.fragmants.LandFragment;
import com.stolbunov.roman.homework_20.ui.fragmants.WaterFragment;

public class FragmentAdapter extends FragmentPagerAdapter {

    public FragmentAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int i) {
        switch (i) {
            case 0:
                return LandFragment.getInstance();
            case 1:
                return WaterFragment.getInstance();
            case 2:
                return AirFragment.getInstance();
            case 3:
                return FireFragment.getInstance();
            default:
                throw new IllegalArgumentException("Unknown fragment index");
        }
    }

    @Override
    public int getCount() {
        return 4;
    }
}
