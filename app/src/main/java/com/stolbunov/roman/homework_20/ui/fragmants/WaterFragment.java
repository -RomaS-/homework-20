package com.stolbunov.roman.homework_20.ui.fragmants;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.stolbunov.roman.homework_20.R;

public class WaterFragment extends Fragment {
    private ImageView imageWater;
    private TextView descriptionWater;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_water, container, false);
        initView(view);
        fillingViewData();
        return view;
    }

    private void initView(View view) {
        imageWater = view.findViewById(R.id.image_water);
        descriptionWater = view.findViewById(R.id.description_water);
    }

    private void fillingViewData() {
        imageWater.setImageResource(R.drawable.water);
        descriptionWater.setText(R.string.description_water);
    }

    public static WaterFragment getInstance() {
        WaterFragment waterFragment = new WaterFragment();
        Bundle args = new Bundle();
        waterFragment.setArguments(args);
        return waterFragment;
    }
}
