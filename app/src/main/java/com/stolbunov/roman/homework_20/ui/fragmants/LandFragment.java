package com.stolbunov.roman.homework_20.ui.fragmants;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.stolbunov.roman.homework_20.R;

public class LandFragment extends Fragment {
    private ImageView imageLand;
    private TextView descriptionLand;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_land, container, false);
        initView(view);
        fillingViewData();
        return view;
    }

    private void initView(View view) {
        imageLand = view.findViewById(R.id.image_land);
        descriptionLand = view.findViewById(R.id.description_land);
    }

    private void fillingViewData() {
        imageLand.setImageResource(R.drawable.land);
        descriptionLand.setText(R.string.description_land);
    }

    public static LandFragment getInstance() {
        LandFragment landFragment = new LandFragment();
        Bundle args = new Bundle();
        landFragment.setArguments(args);
        return landFragment;
    }
}
