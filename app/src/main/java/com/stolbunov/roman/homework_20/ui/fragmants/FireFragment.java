package com.stolbunov.roman.homework_20.ui.fragmants;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.stolbunov.roman.homework_20.R;

public class FireFragment extends Fragment {
    private ImageView imageFire;
    private TextView descriptionFire;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_fire, container, false);
        initView(view);
        fillingViewData();
        return view;
    }

    private void initView(View view) {
        imageFire = view.findViewById(R.id.image_fire);
        descriptionFire = view.findViewById(R.id.description_fire);
    }

    private void fillingViewData() {
        imageFire.setImageResource(R.drawable.fire);
        descriptionFire.setText(R.string.description_fire);
    }

    public static FireFragment getInstance() {
        FireFragment fireFragment = new FireFragment();
        Bundle args = new Bundle();
        fireFragment.setArguments(args);
        return fireFragment;
    }
}
