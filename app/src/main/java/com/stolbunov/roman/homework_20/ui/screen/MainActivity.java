package com.stolbunov.roman.homework_20.ui.screen;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.stolbunov.roman.homework_20.R;
import com.stolbunov.roman.homework_20.ui.adapters.FragmentAdapter;
import com.viewpagerindicator.LinePageIndicator;

public class MainActivity extends AppCompatActivity {
    private int currentFragmentId;

    private ViewPager viewPager;
    private Button btnPrevFragment;
    private Button btnNextFragment;
    private LinePageIndicator indicator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initButton(savedInstanceState);
        initViewPager();
        initIndicator();
    }

    private void initButton(Bundle savedInstanceState) {
        btnPrevFragment = findViewById(R.id.btnPrevFragment);
        btnNextFragment = findViewById(R.id.btnNextFragment);
        btnPrevFragment.setOnClickListener(this::changeCurrentFragment);
        btnNextFragment.setOnClickListener(this::changeCurrentFragment);
        if (savedInstanceState == null) {
            btnPrevFragment.setVisibility(View.INVISIBLE);
        }
    }

    private void initViewPager() {
        viewPager = findViewById(R.id.view_pager);
        viewPager.setAdapter(new FragmentAdapter(getSupportFragmentManager()));
    }

    private void changeCurrentFragment(View view) {
        switch (view.getId()) {
            case R.id.btnNextFragment:
                viewPager.setCurrentItem(currentFragmentId + 1);
                break;
            case R.id.btnPrevFragment:
                viewPager.setCurrentItem(currentFragmentId - 1);
                break;
        }
    }

    private void changeVisibilityButtons(int position) {
        if (position == 0) {
            btnPrevFragment.setVisibility(View.INVISIBLE);
        } else {
            btnPrevFragment.setVisibility(View.VISIBLE);
        }

        if (position == 3) {
            btnNextFragment.setVisibility(View.INVISIBLE);
        } else {
            btnNextFragment.setVisibility(View.VISIBLE);
        }
    }

    private void initIndicator() {
        indicator = findViewById(R.id.pageIndicator);
        indicator.setViewPager(viewPager);
        indicator.setSelectedColor(Color.GREEN);
        indicator.setGapWidth(10);
        indicator.setStrokeWidth(10);
        indicator.setLineWidth(100);
        indicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {
            }

            @Override
            public void onPageSelected(int i) {
                changeVisibilityButtons(i);
                changeIndicatorColor(i);
                currentFragmentId = i;
            }

            @Override
            public void onPageScrollStateChanged(int i) {
            }
        });
    }

    private void changeIndicatorColor(int position) {
        switch (position) {
            case 0:
                indicator.setSelectedColor(getResources().getColor(R.color.colorGreen));
                break;
            case 1:
                indicator.setSelectedColor(getResources().getColor(R.color.colorBlue));
                break;
            case 2:
                indicator.setSelectedColor(getResources().getColor(R.color.colorWhite));
                break;
            case 3:
                indicator.setSelectedColor(getResources().getColor(R.color.colorRed));
                break;
            default:
                throw new RuntimeException("Unknown fragment position");
        }
    }
}
