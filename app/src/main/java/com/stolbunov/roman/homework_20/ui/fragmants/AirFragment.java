package com.stolbunov.roman.homework_20.ui.fragmants;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.stolbunov.roman.homework_20.R;

import java.util.Locale;

public class AirFragment extends Fragment {
    private ImageView imageAir;
    private TextView descriptionAir;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_air, container, false);
        initView(view);
        fillingViewData();
        return view;
    }

    private void initView(View view) {
        imageAir = view.findViewById(R.id.image_air);
        descriptionAir = view.findViewById(R.id.description_air);
    }

    private void fillingViewData() {
        imageAir.setImageResource(R.drawable.air);
        descriptionAir.setText(R.string.description_air);
    }

    public static AirFragment getInstance() {
        AirFragment airFragment = new AirFragment();
        Bundle args = new Bundle();
        airFragment.setArguments(args);
        return airFragment;
    }
}
